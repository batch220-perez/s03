# Activity

# 1. Create a car dictionary with the following keys: brand, model, year of make, color (any values)

car = {
	"brand": "Morris Garage",
	"model": "ZS Alpha",
	"year_of_make": 2022,
	"color": "Meteorite Black"
}



# 2. Print the following statement from the details: "I own a (color) (brand) (model) and it was made in (year_of_make)"

print(f'I own a {car["color"]} {car["brand"]} {car["model"]} and it was made in {car["year_of_make"]}')



# 3. Create a function that gets the square of a number

def square(num):
	return num ** 2

squared_num = square(5)
print(f"The squared number is {squared_num}")



# 4. Create a function that takes one of the following languages as its parameter:

# a. French
# b. Spanish
# c. Japanese

# Depending on which language is given, the function prints "Hello World!" in that language. Add a condition that asks the user to input a valid language if the given parameter does not match any of the above.


# 	"French": "Bonjour le monde",
# 	"Spanish": "Hola Mundo",
# 	"Japanese": "Kon'nichiwa sekai"


def greeting(lang):
	if(lang == "French"):
		return print("Bonjour le monde!")
	elif(lang == "Spanish"):	
		return print("Hola Mundo!")
	elif(lang == "Japanese"):	
		return print("Kon'nichiwa sekai!")
	else:
		return print("Please input a valid language")

greeting("French")
greeting("Spanish")
greeting("Japanese")
greeting("Filipino")


# c.perez